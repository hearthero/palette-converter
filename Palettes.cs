﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.IO;

namespace HeartHero
{
    public class Palettes
    {

        public enum RGBMode
        {
            RGB888,
            RGB565,
            RGB555,
            RGB332
        }

        /// <summary>
        /// Converts a byte array with 16 Bit RGB colors to a System.Drawing.Color array
        /// </summary>
        /// <param name="_binary">A byte array to convert.</param>
        /// <param name="mode">The mode to convert from.</param>
        /// <returns>A color array for image processing.</returns>
        public static Color[] ToDrawingColors(byte[] _binary, RGBMode mode = RGBMode.RGB555)
        {
            List<Color> colors = new List<Color>();
            byte[] rgb888 = convertFromRGBColor(_binary, mode);

            for (int i = 0; i < rgb888.Length; i += 3)
            {
                Color color = Color.FromArgb(rgb888[i], rgb888[i + 1], rgb888[i + 2]);
                colors.Add(color);
            }

            return colors.ToArray();
        }

        /// <summary>
        /// Converts a byte array with 16 Bit RGB colors to a System.Windows.Media.Color array
        /// </summary>
        /// <param name="_binary">A byte array to convert.</param>
        /// <param name="mode">The mode to convert from.</param>
        /// <returns>A color array for image processing.</returns>
        public static System.Windows.Media.Color[] ToMediaColors(byte[] _binary, RGBMode mode = RGBMode.RGB555)
        {
            List<System.Windows.Media.Color> colors = new List<System.Windows.Media.Color>();
            byte[] rgb888 = convertFromRGBColor(_binary, mode);

            for (int i = 0; i < rgb888.Length; i += 3)
            {
                System.Windows.Media.Color color = System.Windows.Media.Color.FromArgb(255, rgb888[i], rgb888[i + 1], rgb888[i + 2]);
                colors.Add(color);
            }

            return colors.ToArray();
        }

        /// <summary>
        /// Converts an array of System.Windows.Media.Color to binary data
        /// </summary>
        /// <param name="colors">The color array to convert.</param>
        /// <param name="mode">The mode to convert to.</param>
        /// <returns>A binary color array of the specified color space.</returns>
        public static byte[] FromMediaColorsToBinary(System.Windows.Media.Color[] colors, RGBMode mode = RGBMode.RGB555)
        {
            List<byte> binary = new List<byte>();

            foreach (System.Windows.Media.Color color in colors)
            {
                byte[] colorBin = ColorToRGB(new byte[3] { color.R, color.G, color.B }, mode);
                binary.AddRange(colorBin);
            }

            return binary.ToArray();
        }

        /// <summary>
        /// Converts an array of System.Drawing.Color to binary data
        /// </summary>
        /// <param name="colors">The color array to convert.</param>
        /// <param name="mode">The mode to convert to.</param>
        /// <returns>A binary color array of the specified color space.</returns>
        public static byte[] FromDrawingColorsToBinary(System.Windows.Media.Color[] colors, RGBMode mode = RGBMode.RGB555)
        {
            List<byte> binary = new List<byte>();

            foreach (System.Windows.Media.Color color in colors)
            {
                byte[] colorBin = ColorToRGB(new byte[3] { color.R, color.G, color.B }, mode);
                binary.AddRange(colorBin);
            }

            return binary.ToArray();
        }

        private static byte[] convertFromRGBColor(byte[] _binary, RGBMode mode = RGBMode.RGB555)
        {

            switch (mode)
            {
                case RGBMode.RGB888:
                    return (_binary.Length % 3 != 0) ? _binary.Take(_binary.Length - (_binary.Length % 3)).ToArray() : _binary;
                case RGBMode.RGB332:
                    return From8BitRGBArray(_binary);
                default:
                    return From16BitRGBArray(_binary, mode);
            }

        }

        /// <summary>
        /// Converts 16 Bit RGB values to 24 Bit RGB values
        /// </summary>
        /// <param name="paletteData">A byte array to convert.</param>
        /// <param name="mode">The mode to convert from</param>
        /// <returns>A byte array of RGB888 values.</returns>
        private static byte[] From16BitRGBArray(byte[] paletteData, RGBMode mode = RGBMode.RGB555)
        {
            if (paletteData == null)
                return null;

            if (paletteData.Length % 2 != 0)
            {
                paletteData = paletteData.Take(paletteData.Length - paletteData.Length % 2).ToArray();
            }

            List<byte> newArr = new List<byte>();

            for (int i = 0; i < paletteData.Length; i += 2)
            {
                byte[] aValue = new byte[2] { paletteData[i], paletteData[i + 1] };


                newArr.AddRange(ColorFromRGB(aValue, mode));

            }
            return newArr.ToArray();
        }

        /// <summary>
        /// Converts 8 Bit RGB values to 24 Bit RGB values
        /// </summary>
        /// <param name="paletteData">A byte array to convert.</param>
        /// <param name="mode">The mode to convert from</param>
        /// <returns>A byte array of RGB888 values.</returns>
        private static byte[] From8BitRGBArray(byte[] paletteData)
        {
            if (paletteData == null)
                return null;

            List<byte> newArr = new List<byte>();

            for (int i = 0; i < paletteData.Length; i++)
            {

                newArr.AddRange(ColorFromRGB(new byte[1] { paletteData[i] }, RGBMode.RGB332));

            }
            return newArr.ToArray();
        }

        /// <summary>
        /// Converts an array of one 16 Bit or 8 Bit color to RGB888
        /// </summary>
        /// <param name="rgb">A byte array to convert.</param>
        /// <param name="mode">The mode to convert from.</param>
        /// <returns>A byte array of one RGB888 value.</returns>
        public static byte[] ColorFromRGB(byte[] rgb, RGBMode mode = RGBMode.RGB555)
        {
            byte[] rgb888 = new byte[3];

            switch (mode)
            {
                case RGBMode.RGB565:
                    {
                        ushort RGB = BitConverter.ToUInt16(rgb);
                        uint r = (uint)((RGB >> (11)) & 31);
                        uint g = (uint)((RGB >> 6) & 63);
                        uint b = (uint)(RGB & 31);
                        uint R = r * 255 / 31;
                        uint G = g * 255 / 63;
                        uint B = b * 255 / 31;
                        rgb888 = BitConverter.GetBytes((R << 16) | (G << 8) | B).Take(3).ToArray();
                    }

                    break;
                case RGBMode.RGB555:
                    {
                        ushort RGB = BitConverter.ToUInt16(rgb);
                        uint r = (uint)((RGB >> (10)) & 31);
                        uint g = (uint)((RGB >> 5) & 31);
                        uint b = (uint)(RGB & 31);
                        uint R = r * 255 / 31;
                        uint G = g * 255 / 31;
                        uint B = b * 255 / 31;
                        rgb888 = BitConverter.GetBytes((uint)((R << 16) | (G << 8) | B)).Take(3).ToArray();
                    }

                    break;
                case RGBMode.RGB332:
                    {
                        byte RGB = rgb[0];
                        uint r = (uint)((RGB >> (6)) & 7);
                        uint g = (uint)((RGB >> 3) & 7);
                        uint b = (uint)(RGB & 3);
                        uint R = r * 255 / 7;
                        uint G = g * 255 / 7;
                        uint B = b * 255 / 3;
                        rgb888 = BitConverter.GetBytes((uint)((R << 16) | (G << 8) | B)).Take(3).ToArray();
                    }

                    break;
            }

            return rgb888;

        }

        /// <summary>
        /// Converts an array of one RGB888 color to a lower colorspace
        /// </summary>
        /// <param name="rgb888">A RGB888 byte array to convert.</param>
        /// <param name="mode">The mode to convert to.</param>
        /// <returns>A byte array of 16 Bit or 8 Bit colors.</returns>
        public static byte[] ColorToRGB(byte[] rgb888, RGBMode mode = RGBMode.RGB555)
        {
            uint RGB = BitConverter.ToUInt32(new byte[4] { rgb888[0], rgb888[1], rgb888[2], 0 });

            uint R = (RGB >> 16) & 0xFF;
            uint G = (RGB >> 8) & 0xFF;
            uint B = RGB & 0xFF;

            byte[] rgbOutput = new byte[0];

            switch (mode)
            {
                case RGBMode.RGB555:
                    {
                        uint r = R * 31 / 255;
                        uint g = G * 31 / 255;
                        uint b = B * 31 / 255;
                        rgbOutput = BitConverter.GetBytes((ushort)(r << (10) | (g << 5) | b));
                    }
                    break;
                case RGBMode.RGB565:
                    {
                        uint r = R * 31 / 255;
                        uint g = G * 63 / 255;
                        uint b = B * 31 / 255;
                        rgbOutput = BitConverter.GetBytes((ushort)(r << (11) | (g << 6) | b));
                    }
                    break;
                case RGBMode.RGB332:
                    {
                        uint r = R * 7 / 255;
                        uint g = G * 7 / 255;
                        uint b = B * 3 / 255;
                        rgbOutput = new byte[1] { (byte)(r << (6) | (g << 3) | b) };
                    }
                    break;
                case RGBMode.RGB888:
                    return rgb888;
            }

            return rgbOutput;
        }

        /// <summary>
        /// Reads data from a PAL File (Microsoft RIFF)
        /// </summary>
        /// <param name="filename">The path of the file.</param>
        /// <returns>A byte array of RGB888 values.</returns>
        public static byte[] LoadPal(string filename)
        {
            List<byte> colorBin = new List<byte>();
            FileStream stream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read);
            stream.Seek(0x16, SeekOrigin.Begin);

            using (BinaryReader br = new BinaryReader(stream))
            {
                
                short palEntries = br.ReadInt16();

                // Colors
                for (int i = 0; i < palEntries; i++)
                {
                    byte red = br.ReadByte();
                    byte green = br.ReadByte();
                    byte blue = br.ReadByte();
                    byte flags = br.ReadByte(); // always 0x00
                    colorBin.AddRange(new byte[3] { red, green, blue });
                }
            }
            return colorBin.ToArray();
        }

        /// <summary>
        /// Saves binary RGB data into a PAL File (Microsoft RIFF)
        /// </summary>
        /// <param name="filename">The path for the generated file.</param>
        /// <param name="colors">A byte array of RGB888 colors.</param>
        public static void SavePal(string filename, byte[] colors)
        {
            // Calculate file length
            int colorLength = colors.Length / 3;
            int totalLength = 4 + 4 + 4 + 4 + 2 + 2 + colorLength * 4;

            FileStream stream = new FileStream(filename, FileMode.Create, FileAccess.Write, FileShare.None);
            using (BinaryWriter bw = new BinaryWriter(stream))
            {
                // RIFF header
                bw.Write("RIFF");
                bw.Write(totalLength);
                bw.Write("PAL data");

                // Data chunk
                bw.Write(colorLength * 4 + 4);
                bw.Write(0x0300);
                bw.Write((short)colorLength);

                // Colors
                for (int i=0; i < colors.Length; i += 3)
                {
                    bw.Write(colors[i]);
                    bw.Write(colors[i + 1]);
                    bw.Write(colors[i + 2]);
                    bw.Write((byte)0);
                }

            }
        }

        /// <summary>
        /// Returns a hex string for binary 24 bit RGB data
        /// </summary>
        /// <param name="c">The Color to retrieve hex information from.</param>
        /// <returns>A hex code for the specified color.</returns>
        public static string ColorToHex(byte[] bin)
        {
            return "#" + bin[0].ToString("X2") + bin[1].ToString("X2") + bin[2].ToString("X2");
        }

        /// <summary>
        /// Returns a hex string for a System.Drawing.Color
        /// </summary>
        /// <param name="c">The Color to retrieve hex information from.</param>
        /// <returns>A hex code for the specified color.</returns>
        public static string ColorToHex(Color c)
        {
            return "#" + c.R.ToString("X2") + c.G.ToString("X2") + c.B.ToString("X2");
        }

        /// <summary>
        /// Returns a hex string for a System.Windows.Media.Color
        /// </summary>
        /// <param name="c">The Color to retrieve hex information from.</param>
        /// <returns>A hex code for the specified color.</returns>
        public static string ColorToHex(System.Windows.Media.Color c)
        {
            return "#" + c.R.ToString("X2") + c.G.ToString("X2") + c.B.ToString("X2");
        }

        public static System.Windows.Media.Color ColorFromHex(string hex)
        {
            byte R = Convert.ToByte(hex.Substring(1, 2), 16);
            byte G = Convert.ToByte(hex.Substring(3, 2), 16);
            byte B = Convert.ToByte(hex.Substring(5, 2), 16);
            return System.Windows.Media.Color.FromRgb(R, G, B);
        }


    }
}
