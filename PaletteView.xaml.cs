﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace HeartHero
{
    /// <summary>
    /// Interaktionslogik für PaletteView.xaml
    /// </summary>
    public partial class PaletteView : UserControl, INotifyPropertyChanged
    {

        public static readonly DependencyProperty ColorsProperty =
            DependencyProperty.Register("Colors", typeof(Color[]), typeof(PaletteView));

        public static readonly DependencyProperty ColumnCountProperty =
            DependencyProperty.Register("ColumnCount", typeof(int), typeof(PaletteView), new PropertyMetadata(null));

        public Color[] Colors
        {
            get
            {
                return GetValue(ColorsProperty) as Color[];
            }
            set
            {
                SetValue(ColorsProperty, value);
                Mouse.OverrideCursor = Cursors.Wait;
                colorList.Visibility = Visibility.Hidden;
                colorList.UpdateLayout();

                using (Dispatcher.DisableProcessing())
                {
                    int length = (Colors.Length > 255) ? 256 : Colors.Length;
                    PaletteColor[] range = new PaletteColor[length];
                    for (int i = 0; i < length; i++)
                    {
                        range[i] = new PaletteColor() { index = i, color = new SolidColorBrush(Colors[i]) };
                    }
                    items.Reset(range);
                };

                Mouse.OverrideCursor = null;
                colorList.Visibility = Visibility.Visible;
            }
        }

        SmartCollection<PaletteColor> items = new SmartCollection<PaletteColor>();

        public int ColumnCount
        {
            get { return (int)GetValue(ColumnCountProperty); }
            set { SetValue(ColumnCountProperty, value); NotifyPropertyChanged("ColumnCount"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public PaletteView()
        {
            InitializeComponent();
            colorList.ItemsSource = items;
        }

        private void ColorTile_Click(object sender, RoutedEventArgs e)
        {
            Button btn = ((Button)sender);
            PaletteColor binding = (PaletteColor)btn.DataContext;
            Clipboard.SetText(Palettes.ColorToHex(binding.color.Color));
        }

        private void Copy_Clicked(object sender, RoutedEventArgs e)
        {
            MenuItem item = (MenuItem)(sender);
            Button btn = (Button)(item.CommandTarget);
            PaletteColor binding = (PaletteColor)(btn).DataContext;

            Clipboard.SetText(Palettes.ColorToHex(binding.color.Color));
        }

        private void Paste_Clicked(object sender, RoutedEventArgs e)
        {
            MenuItem item = (MenuItem)(sender);
            Button btn = (Button)(item.CommandTarget);
            PaletteColor binding = (PaletteColor)(btn).DataContext;

            string clipboardText = Clipboard.GetText();

            if (clipboardText.Length == 7 && clipboardText.StartsWith('#'))
            {
                Colors[binding.index] = Palettes.ColorFromHex(Clipboard.GetText());
                items[binding.index].color = new SolidColorBrush(Colors[binding.index]);
            }


        }
    }

    public class PaletteColor : INotifyPropertyChanged
    {
        public int index { get; set; }
        public SolidColorBrush color { get => _color; set { _color = value; OnPropertyChanged(); } }

        public string indexString
        {
            get { return $"Index (DEC): {index}\r\nIndex (HEX): {index.ToString("X8")}\r\nColor Code: {Palettes.ColorToHex(color.Color)}"; }
        }

        private SolidColorBrush _color;

        //INotifyPropertyChanged implementation
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }

    public class SmartCollection<T> : ObservableCollection<T>
    {
        public SmartCollection()
            : base()
        {
        }

        public SmartCollection(IEnumerable<T> collection)
            : base(collection)
        {
        }

        public SmartCollection(List<T> list)
            : base(list)
        {
        }

        public void AddRange(IEnumerable<T> range)
        {
            foreach (var item in range)
            {
                Items.Add(item);
            }

            this.OnPropertyChanged(new PropertyChangedEventArgs("Count"));
            this.OnPropertyChanged(new PropertyChangedEventArgs("Item[]"));
            this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        public void Reset(IEnumerable<T> range)
        {
            this.Items.Clear();

            AddRange(range);
        }
    }

}
