> Why have Hex editors no function to search for colors when they are
> essentially just numbers?

This question bothered me a while. Knowing where palettes are located within files is one of the fundamentals for hacking graphical resources. But even if you know the palette location, editing palettes can be an unnecessarily long process.

I decided to create a tool for speeding things up and making palette finding and editing a little bit easier. Here is a use case example:
![The color search window](https://i.imgur.com/2jULyHY.png)

We opened a file from which we know that it contains graphics and palettes. Searching for magenta (#FF00FF) is often a good start since magenta is usually a color that gets replaced for the alpha channel. In this example we found two entries within this file. So let's check them:
|  ![Palette number 1](https://i.imgur.com/4LvHWOW.png)| ![Palette number 2](https://i.imgur.com/bXl2XnG.png) |
|--|--|

It seems we found two entire palettes with ease! You can directly edit palettes within the Palette View by right clicking on a field and copying/pasting hex representations of colors. That means you can also copy colors directly from image editors to the palette view and vice versa.

Once you are done you can save over the original or create a new file.

The Palette Converter supports the most common RGB formats for games. (RGB555 is typical for GBA/DS games, RGB565 is used by DXT textures, etc.)

When storing files in a lower colorspace than RGB888 a lossy conversion happens.

This is a normal limitation since information has to be omitted for such formats. (They store less colors after all)

If you want to edit one palette multiple times I advise to keep a RGB888 copy to work with from which you can export to other colorspaces any time you want.

This software also comes with my Palettes class for C# that you may use as you like! :)

### External Resources:
- Fugue Icons by Yusuke Kamiyamane
