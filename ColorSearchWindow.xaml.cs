﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace HeartHero
{
    /// <summary>
    /// Interaktionslogik für ColorSearchWindow.xaml
    /// </summary>
    public partial class ColorSearchWindow : Window
    {

        public string BinaryFile;
        List<ColorSearchEntry> entries = new List<ColorSearchEntry>();
        public int? selectedOffset = null;

        public ColorSearchWindow()
        {
            InitializeComponent();
            ResultsView.ItemsSource = entries;
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
            e.Cancel = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void StartSearchButton_Click(object sender, RoutedEventArgs e)
        {
            Color searchColor = (Color)ColorSelect.SelectedColor;
            Mouse.OverrideCursor = Cursors.Wait;
            StartSearchButton.IsEnabled = false;
            ColorSelect.IsEnabled = false;
            DepthSelect.IsEnabled = false;
            ResultsBox.Header = "Results for " + Palettes.ColorToHex(searchColor);

            using (Dispatcher.DisableProcessing())
            {
                entries.Clear();

                if (check24Bit.IsChecked == true)
                {
                    search24(searchColor);
                }

                if (check16Bit.IsChecked == true)
                {
                    search16(searchColor);
                }

                if (check8Bit.IsChecked == true)
                {
                    search8(searchColor);
                }

                ResultsView.Items.Refresh();
            }

            StartSearchButton.IsEnabled = true;
            ColorSelect.IsEnabled = true;
            DepthSelect.IsEnabled = true;
            Mouse.OverrideCursor = null;
            ResultsBox.Header += $" ({entries.Count} entries)";
        }

        private void search8(Color searchColor)
        {
            using (BinaryReader reader = new BinaryReader(File.Open(BinaryFile, FileMode.Open)))
            {
                while (reader.BaseStream.Position < reader.BaseStream.Length)
                {
                    int i = (int)reader.BaseStream.Position;
                    byte[] val = new byte[1] { reader.ReadByte() };
                    byte[] rgb332 = Palettes.ColorFromRGB(val, Palettes.RGBMode.RGB332);

                    if (Palettes.ColorToHex(rgb332) == Palettes.ColorToHex(searchColor))
                    {
                        entries.Add(new ColorSearchEntry() { Offset = i, Type = "RGB 332" });
                    }
                    
                }
            }
        }

        private void search16(Color searchColor)
        {
            using (BinaryReader reader = new BinaryReader(File.Open(BinaryFile, FileMode.Open)))
            {
                while (reader.BaseStream.Position < reader.BaseStream.Length)
                {
                    int i = (int)reader.BaseStream.Position;
                    byte[] val = reader.ReadBytes(2);
                    byte[] rgb555 = Palettes.ColorFromRGB(val, Palettes.RGBMode.RGB555);
                    byte[] rgb565 = Palettes.ColorFromRGB(val, Palettes.RGBMode.RGB565);
                    bool found555 = (Palettes.ColorToHex(rgb555) == Palettes.ColorToHex(searchColor));
                    bool found565 = (Palettes.ColorToHex(rgb565) == Palettes.ColorToHex(searchColor));

                    if (val.Length < 2) break;

                    if (found555 && found565)
                    {
                        entries.Add(new ColorSearchEntry() { Offset = i, Type = "RGB 555 / RGB 565" });
                    }
                    else if (found555)
                    {
                        entries.Add(new ColorSearchEntry() { Offset = i, Type = "RGB 555" });
                    }
                    else if (found565)
                    {
                        entries.Add(new ColorSearchEntry() { Offset = i, Type = "RGB 565" });
                    }

                }
            }
        }

        private void search24(Color searchColor)
        {
            using (BinaryReader reader = new BinaryReader(File.Open(BinaryFile, FileMode.Open)))
            {
                while (reader.BaseStream.Position < reader.BaseStream.Length)
                {
                    int i = (int)reader.BaseStream.Position;
                    byte[] val = reader.ReadBytes(3);

                    if (val.Length < 3) break;

                    if (Palettes.ColorToHex(val) == Palettes.ColorToHex(searchColor))
                    {
                        entries.Add(new ColorSearchEntry() { Offset = i, Type = "RGB 888" });
                    }

                }
            }
        }

        private void ColorSearch_Activated(object sender, EventArgs e)
        {
            selectedOffset = null;
            ResultsView.Items.Refresh();
            if (!Clipboard.ContainsText()) return;

            string clipboardText = Clipboard.GetText();
            if (clipboardText.Length == 7 && clipboardText.StartsWith('#'))
            {
                ColorSelect.SelectedColor = (Color)ColorConverter.ConvertFromString(clipboardText);
            }
        }

        private void ColorSearch_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            entries.Clear();
        }

        private void ResultsView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ColorSearchEntry entry = ((sender as ListBox).SelectedItem as ColorSearchEntry);
            if (entry != null)
            {
                selectedOffset = entry.Offset;
                ResultsView.SelectedItem = null;
                this.Close();
            }
        }
    }

    public enum SearchMode
    {
        Any,
        Depth16,
        Depth24
    }

    public class ColorSearchEntry
    {
        private int offset;
        public string OffsetHex { get; set; }

        public int Offset {
            get { return offset; }
            set { offset = value; OffsetHex = value.ToString("X8"); }
        }
        public string Type { get; set; }
    }

}
