﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace HeartHero
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        byte[] Binary = new byte[0];
        ColorSearchWindow SearchWindow = new ColorSearchWindow();
        OpenFileDialog binaryFileDialog = new OpenFileDialog();
        OpenFileDialog paletteFileDialog = new OpenFileDialog();
        SaveFileDialog exportFileDialog = new SaveFileDialog();

        string binaryFile
        {
            get { return binaryFileDialog.FileName; }
            set
            {
                FilePath.Text = binaryFile;
            }
        }

        string fileInfo
        {
            get { return fileStatus.Text; } set { fileStatus.Text = value; }
        }

        public MainWindow()
        {
            InitializeComponent();
            ApplicationCommands.Paste.CanExecuteChanged += new EventHandler(Paste_CanExecuteChanged);

            fileInfo = "No file opened";

            paletteFileDialog.Filter = "Palette Files(*.ACT;*.PAL)|*.ACT;*.PAL";


            radioToRGB888.IsChecked = true;
            radioFromRGB555.IsChecked = true;
        }

        private Color Invert(Color originalColor)
        {
            Color invertedColor = new Color();
            invertedColor.ScR = 1.0F - originalColor.ScR;
            invertedColor.ScG = 1.0F - originalColor.ScG;
            invertedColor.ScB = 1.0F - originalColor.ScB;
            invertedColor.ScA = originalColor.ScA;
            return invertedColor;
        }

        private void Paste_CanExecuteChanged(object sender, EventArgs e)
        {
            if (Clipboard.ContainsText())
            {

                for (int i = 0; i < 10; i++)
                {
                    try
                    {
                        string clipboardText = Clipboard.GetText();

                        if (clipboardText.Length == 7 && clipboardText.StartsWith('#'))
                        {
                            Color color = Palettes.ColorFromHex(clipboardText);
                            clipboardStatus.Background = new SolidColorBrush(color);
                            clipboardStatus.Foreground = new SolidColorBrush(Invert(color));
                            clipboardStatus.Text = "Clipboard HEX: " + clipboardText;
                            fromClipboardHex.IsEnabled = false;
                        }
                        else
                        {
                            clipboardStatus.Background = null;
                            clipboardStatus.Foreground = new SolidColorBrush(Colors.Black);
                            clipboardStatus.Text = Encoding.UTF8.GetByteCount(Clipboard.GetText()) + " Bytes in clipboard";
                            fromClipboardHex.IsEnabled = true;
                        }
                        return;
                    }
                    catch (COMException ex) { const uint CLIPBRD_E_CANT_OPEN = 0x800401D0; if ((uint)ex.ErrorCode != CLIPBRD_E_CANT_OPEN) throw; }
                    System.Threading.Thread.Sleep(10);
                }

            } else
            {
                clipboardStatus.Text = "No text data in clipboard.";
                fromClipboardHex.IsEnabled = false;
            }

        }

        private void updatePaletteView()
        {

            Palettes.RGBMode mode = Palettes.RGBMode.RGB888;
            if ((bool)radioFromRGB565.IsChecked) { mode = Palettes.RGBMode.RGB565; }
            if ((bool)radioFromRGB555.IsChecked) { mode = Palettes.RGBMode.RGB555; }
            if ((bool)radioFromRGB332.IsChecked) { mode = Palettes.RGBMode.RGB332; }

            Color[] RGB = Palettes.ToMediaColors(Binary, mode);
            PaletteView.Colors = RGB;
            PaletteViewBox.Header = $"Palette View ({RGB.Length} Colors)";
        }

        private void To8BitRadioChecked(object sender, RoutedEventArgs e)
        {
            radioExportACT.IsEnabled = false;
            radioExportPAL.IsEnabled = false;
            radioExportBIN.IsChecked = true;
        }

        private void To16BitRadioChecked(object sender, RoutedEventArgs e)
        {
            radioExportACT.IsEnabled = false;
            radioExportPAL.IsEnabled = false;
            radioExportBIN.IsChecked = true;
        }

        private void To24BitRadioChecked(object sender, RoutedEventArgs e)
        {
            radioExportACT.IsEnabled = true;
            radioExportPAL.IsEnabled = true;
        }

        private void SearchColorButton_Click(object sender, RoutedEventArgs e)
        {
            SearchWindow.BinaryFile = binaryFile;
            SearchWindow.ShowDialog();

            if (SearchWindow.selectedOffset != null)
            {
                FileOffset.Text = ((int)SearchWindow.selectedOffset).ToString("X8");
                FileOffset.Focus();
                FileOffset.SelectAll();
            }
        }

        private void fromBinary_Click(object sender, RoutedEventArgs e)
        {
            if (binaryFileDialog.ShowDialog() == true)
            {
                binaryFile = binaryFileDialog.FileName;
                searchColor.IsEnabled = true;
                generatePalette.IsEnabled = true;
            }
        }

        private void fromPaletteFile_Click(object sender, RoutedEventArgs e)
        {
            if (paletteFileDialog.ShowDialog() == true)
            {
                switch (Path.GetExtension(paletteFileDialog.FileName))
                {
                    case ".pal":
                        Binary = Palettes.LoadPal(paletteFileDialog.FileName);
                        break;
                    case ".act":
                        Binary = File.ReadAllBytes(paletteFileDialog.FileName);
                        break;
                }

                fileInfo = $"Palette file loaded ({Binary.Length} Bytes)";

                if (radioFromRGB888.IsChecked == true)
                {
                    updatePaletteView();
                } else
                {
                    radioFromRGB888.IsChecked = true;
                }
                
            }
        }

        private void fromClipboardHex_Click(object sender, RoutedEventArgs e)
        {
            string clipboardText = Clipboard.GetText().Replace(" ", string.Empty);
            List<byte> textData = new List<byte>();

            try
            {
                for (int i = 0; i < clipboardText.Length; i += 2)
                {
                    byte hexValue = Convert.ToByte(clipboardText.Substring(i, 2), 16);
                    textData.Add(hexValue);
                }
                Binary = textData.ToArray();
                updatePaletteView();
                fileInfo = $"Clipboard data loaded ({Binary.Length} Bytes)";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Not a valid hex string.\r\n" + ex, "Invalid format", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }

        }

        private void generatePaletteButton_Click(object sender, RoutedEventArgs e)
        {
            int offset = Convert.ToInt32(FileOffset.Text, 16);
            int length = Convert.ToInt32(FileLength.Text, 16);
            using (FileStream fs = new FileStream(binaryFile, FileMode.Open, FileAccess.Read))
            {
                if (offset < fs.Length)
                {
                    fs.Seek(offset, SeekOrigin.Begin);
                } else
                {
                    offset = 0;
                    FileOffset.Text = offset.ToString("X8");
                }

                if (offset + length > fs.Length)
                {
                    Binary = new byte[fs.Length - offset];
                    FileLength.Text = (fs.Length - offset).ToString("X8");
                } else
                {
                    Binary = new byte[length];
                }

                fs.Read(Binary, 0, Binary.Length);

            }

            fileInfo = $"Binary file loaded ({Binary.Length} Bytes)";
            updatePaletteView();
        }

        private void FromRGB_Checked(object sender, RoutedEventArgs e)
        {
            updatePaletteView();
        }

        private byte[] createPalette()
        {
            byte[] newPalette;

            if (radioToRGB555.IsChecked == true)
            {
                newPalette = Palettes.FromMediaColorsToBinary(PaletteView.Colors, Palettes.RGBMode.RGB555);
            }
            else if (radioToRGB565.IsChecked == true)
            {
                newPalette = Palettes.FromMediaColorsToBinary(PaletteView.Colors, Palettes.RGBMode.RGB565);
            }
            else if (radioToRGB332.IsChecked == true)
            {
                newPalette = Palettes.FromMediaColorsToBinary(PaletteView.Colors, Palettes.RGBMode.RGB332);
            }
            else
            {
                newPalette = Palettes.FromMediaColorsToBinary(PaletteView.Colors, Palettes.RGBMode.RGB888);
            }

            return newPalette;
        }

        private void toFile_Clicked(object sender, RoutedEventArgs e)
        {
            byte[] newPalette = createPalette();

            if (radioExportACT.IsChecked == true)
            {
                exportFileDialog.Filter = "Adobe Color Table (*.ACT)|*.ACT";
                if (exportFileDialog.ShowDialog() == true)
                {
                    File.WriteAllBytes(exportFileDialog.FileName, newPalette);
                }
            } else if (radioExportPAL.IsChecked == true)
            {
                exportFileDialog.Filter = "Palette File (*.PAL)|*.PAL";
                if (exportFileDialog.ShowDialog() == true)
                {
                    Palettes.SavePal(exportFileDialog.FileName, newPalette);
                }
            } else
            {
                exportFileDialog.Filter = "All files (*.*)|*.*";
                if (exportFileDialog.ShowDialog() == true)
                {
                    Palettes.SavePal(exportFileDialog.FileName, newPalette);
                }
            }


        }

        private void toClipboard_Clicked(object sender, RoutedEventArgs e)
        {
            byte[] newPalette = createPalette();

            Clipboard.SetText(Convert.ToHexString(newPalette));

        }
    }
}
